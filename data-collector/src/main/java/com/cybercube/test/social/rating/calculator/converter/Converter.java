package com.cybercube.test.social.rating.calculator.converter;

import com.amazonaws.services.sqs.model.Message;
import com.cybercube.test.core.sqs.model.User;
import com.cybercube.test.data.collector.dto.UserDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Base64;

@Component
public class Converter {

    @Value("${seed:0.5}")
    private double seed;

    private static String toString(Serializable serializable) throws IOException {
        try (var byteArrayOutputStream = new ByteArrayOutputStream();
             var objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {
            objectOutputStream.writeObject(serializable);
            return Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray());
        }
    }

    public Message toMessage(UserDto dto) throws IOException {
        var user = User.builder()
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .age(dto.getAge())
                .seed(seed)
                .build();

        var string = toString(user);
        var message = new Message();
        message.setBody(string);

        return message;
    }
}
