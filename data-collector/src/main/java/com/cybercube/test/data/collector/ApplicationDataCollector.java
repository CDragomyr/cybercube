package com.cybercube.test.data.collector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@PropertySource("classpath:application.properties")
@EnableRedisRepositories("com.cybercube.test.core.repository")
@SpringBootApplication(scanBasePackages = {"com.cybercube.test"})
public class ApplicationDataCollector {

    public static void main(String[] args) {
        SpringApplication.run(ApplicationDataCollector.class);
    }
}
