package com.cybercube.test.data.collector.controller;

import com.cybercube.test.core.service.SqsService;
import com.cybercube.test.social.rating.calculator.converter.Converter;
import com.cybercube.test.data.collector.dto.UserDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@Slf4j
@RequiredArgsConstructor
@RestController("/ws/user")
public class UserController {

    private final SqsService sqsService;
    private final Converter converter;

    @PostMapping
    public void sendUserData(@RequestBody UserDto userDto) throws IOException {
        var message = converter.toMessage(userDto);
        sqsService.send(message);
    }
}
