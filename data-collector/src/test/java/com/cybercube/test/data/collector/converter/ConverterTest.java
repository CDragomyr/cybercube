package com.cybercube.test.data.collector.converter;

import com.amazonaws.services.sqs.model.Message;
import com.cybercube.test.data.collector.dto.UserDto;
import com.cybercube.test.social.rating.calculator.converter.Converter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

class ConverterTest {

    private final Converter converter = new Converter();

    @BeforeEach
    public void beforeAll() {
        ReflectionTestUtils.setField(converter, "seed", 0.5);
    }

    @Test
    void toMessage() throws IOException {
        var dto = new UserDto();
        dto.setAge(10);
        dto.setFirstName("firstname");
        dto.setLastName("lastname");

        Message message = converter.toMessage(dto);
        assertThat(message).isNotNull().extracting(Message::getBody).isNotEqualTo("");
    }
}