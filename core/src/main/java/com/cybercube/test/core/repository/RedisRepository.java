package com.cybercube.test.core.repository;

import com.cybercube.test.core.entity.UserEntity;

import java.util.List;

public interface RedisRepository {

    List<UserEntity> findAll();
    void add(UserEntity entity);
    void update(UserEntity entity);
}
