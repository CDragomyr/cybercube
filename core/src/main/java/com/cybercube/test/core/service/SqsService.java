package com.cybercube.test.core.service;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SqsService {

    private final AmazonSQS amazonSQS;

    @Value("${sqs.queue.name}")
    private String queueName;

    public void send(Message message) {
        var queueUrl = getSqsQueueUrl();
        var sendMessageRequest = new SendMessageRequest(queueUrl, message.getBody());
        sendMessageRequest.setMessageAttributes(message.getMessageAttributes());

        amazonSQS.sendMessage(sendMessageRequest);
    }

    public List<Message> receive() {
        var receiveMessageRequest = buildReceiveMessageRequest();
        return amazonSQS.receiveMessage(receiveMessageRequest).getMessages();
    }

    public void remove(String receiptHandle) {
        var queueUrl = getSqsQueueUrl();
        amazonSQS.deleteMessage(queueUrl, receiptHandle);
    }

    private String getSqsQueueUrl() {
        return amazonSQS.getQueueUrl(queueName).getQueueUrl();
    }

    private ReceiveMessageRequest buildReceiveMessageRequest() {
        var queueUrl = getSqsQueueUrl();
        return new ReceiveMessageRequest(queueUrl)
                .withMaxNumberOfMessages(1)
                .withWaitTimeSeconds(3);
    }
}
