package com.cybercube.test.core.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SqsConfiguration {

    @Value("${sqs.host:loclhost}")
    private String host;
    @Value("${sqs.region:elasticmq}")
    private String region;
    @Value("${sqs.secret.key:x}")
    private String secretKey;
    @Value("${sqs.access.key:x}")
    private String accessKey;
    @Value("${sqs.queue.name:test}")
    private String queueName;

    @Bean
    public AmazonSQS amazonSQS() {
        var amazonSQS = AmazonSQSClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(host, region))
                .build();
        amazonSQS.createQueue(queueName);

        return amazonSQS;
    }
}
