package com.cybercube.test.core.entity;

import lombok.Data;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@Data
@RedisHash("User")
public class UserEntity implements Serializable {

    private String firstName;
    private String lastName;
    private String nickname;
    private int age;
    private double score;
}
