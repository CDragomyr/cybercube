package com.cybercube.test.core.repository;

import com.cybercube.test.core.entity.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class RedisRepositoryImpl implements RedisRepository {

    private static final String HASH_KEY = "User";

    private final RedisTemplate<String, UserEntity> redisTemplate;

    private HashOperations<String, Object, UserEntity> hashOperations;

    @PostConstruct
    private void postConstruct() {
        this.hashOperations = redisTemplate.opsForHash();
    }

    @Override
    public List<UserEntity> findAll() {
        return new ArrayList<>(hashOperations.entries(HASH_KEY).values());
    }

    @Override
    public void add(UserEntity entity) {
        hashOperations.put(HASH_KEY, entity.getNickname(), entity);
    }

    @Override
    public void update(UserEntity entity) {
        add(entity);
    }
}
