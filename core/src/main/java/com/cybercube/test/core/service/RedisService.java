package com.cybercube.test.core.service;

import com.cybercube.test.core.entity.UserEntity;
import com.cybercube.test.core.repository.RedisRepository;
import com.cybercube.test.core.sqs.model.User;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RedisService {

    private final RedisRepository redisRepository;

    public void save(User user, double score) {
        var entity = new UserEntity();
        entity.setFirstName(user.getFirstName());
        entity.setLastName(user.getLastName());
        entity.setNickname(String.join("-", user.getFirstName(), user.getLastName()));
        entity.setAge(user.getAge());
        entity.setScore(score);

        redisRepository.add(entity);
    }

    public void update(UserEntity entity, double score) {
        entity.setScore(score);

        redisRepository.update(entity);
    }

    public List<UserEntity> findRedisEntitiesByFirstNameAndLastNameAndAge(String firstName, String lastName) {
        return redisRepository.findAll().stream()
                .filter(entity -> StringUtils.equals(entity.getFirstName(), firstName) &&
                        StringUtils.equals(entity.getLastName(), lastName))
                .collect(Collectors.toList());
    }
}
