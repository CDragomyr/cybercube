package com.cybercube.test.core.sqs.model;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

@Getter
@Builder
public class User implements Serializable {

    private String firstName;
    private String lastName;
    private int age;
    private double seed;
}
