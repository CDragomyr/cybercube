package com.cybercube.test.social.rating.calculator;

import com.cybercube.test.social.rating.calculator.listener.SqsListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@PropertySource("classpath:application.properties")
@EnableRedisRepositories("com.cybercube.test.core.repository")
@SpringBootApplication(scanBasePackages = {"com.cybercube.test"})
public class SocialRatingCalculatorApplication {

    public static void main(String[] args) {
        var applicationContext = SpringApplication.run(SocialRatingCalculatorApplication.class);
        var sqsListener = applicationContext.getBean(SqsListener.class);
        sqsListener.listen();
    }
}
