package com.cybercube.test.social.rating.calculator.listener;

import com.cybercube.test.core.service.RedisService;
import com.cybercube.test.core.service.SqsService;
import com.cybercube.test.core.sqs.model.User;
import com.cybercube.test.social.rating.calculator.converter.Converter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.IOException;

@Slf4j
@Service
@RequiredArgsConstructor
public class SqsListener {

    private final SqsService sqsService;
    private final Converter converter;
    private final RedisService redisService;

    private static double calculateScore(User user) {
        return (double) user.getAge() * user.getSeed();
    }

    public void listen() {
        while (true) {
            var messages = sqsService.receive();
            messages.forEach(message -> {
                try {
                    var user = (User) converter.fromMessage(message);
                    var score = calculateScore(user);
                    var entities = redisService.findRedisEntitiesByFirstNameAndLastNameAndAge(user.getFirstName(), user.getLastName());
                    if (CollectionUtils.isEmpty(entities)) {
                        log.info("Create new record: {} {} has {} score", user.getLastName(), user.getLastName(), score);
                        redisService.save(user, score);
                    } else {
                        log.info("Found {} record(s)", entities.size());
                        log.info("Update user: {} {} has {} score", user.getLastName(), user.getLastName(), score);
                        redisService.update(entities.get(0), score);
                    }
                    sqsService.remove(message.getReceiptHandle());
                } catch (IOException | ClassNotFoundException e) {
                    log.error("Error occurred: {}", e.getMessage(), e);
                }
            });
        }
    }
}
