package com.cybercube.test.social.rating.calculator.converter;

import com.amazonaws.services.sqs.model.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Base64;

@Slf4j
@Component
public class Converter {

    @SuppressWarnings("unchecked")
    private static <T> T fromString(String string) throws IOException, ClassNotFoundException {
        byte[] data = Base64.getDecoder().decode(string);
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(data))) {
            return (T) objectInputStream.readObject();
        }
    }

    public <T> T fromMessage(Message message) throws IOException, ClassNotFoundException {
        var body = message.getBody();
        return fromString(body);
    }
}
