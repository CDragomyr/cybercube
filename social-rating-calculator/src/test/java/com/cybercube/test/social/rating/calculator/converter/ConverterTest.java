package com.cybercube.test.social.rating.calculator.converter;

import com.amazonaws.services.sqs.model.Message;
import com.cybercube.test.core.sqs.model.User;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

class ConverterTest {

    private static final String MESSAGE = "rO0ABXNyACZjb20uY3liZXJjdWJlLnRlc3QuY29yZS5zcXMubW9kZWwuVXNlctWRWIupbWz2A" +
            "gAESQADYWdlRAAEc2VlZEwACWZpcnN0TmFtZXQAEkxqYXZhL2xhbmcvU3RyaW5nO0wACGxhc3ROYW1lcQB+A" +
            "AF4cAAAAAo/4AAAAAAAAHQACWZpcnN0bmFtZXQACGxhc3RuYW1l";
    private final Converter converter = new Converter();

    @Test
    void fromMessage() throws IOException, ClassNotFoundException {
        var message = new Message();
        message.setBody(MESSAGE);

        var user = (User) converter.fromMessage(message);
        assertThat(user).isNotNull()
                .extracting(User::getFirstName, User::getLastName, User::getAge, User::getSeed)
                .containsExactly("firstname", "lastname", 10, 0.5);
    }
}